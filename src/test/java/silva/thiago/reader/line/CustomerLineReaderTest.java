package silva.thiago.reader.line;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import silva.thiago.file.reader.line.CustomerLineReader;
import silva.thiago.file.reader.line.LineReader;
import silva.thiago.model.Customer;
import silva.thiago.model.CustomerImpl;
import silva.thiago.model.SalesFile;

import java.util.Arrays;
import java.util.Iterator;

public class CustomerLineReaderTest {
    private Iterator<String> line;
    private SalesFile salesFileMock;
    private LineReader lineReader;

    @BeforeEach
    public void setup() {
        String customerLine = "2345675434544345çJose da SilvaçRural";

        this.lineReader = new CustomerLineReader();
        this.line = Arrays.asList(customerLine.split("ç")).iterator();
        this.salesFileMock = Mockito.mock(SalesFile.class);
    }

    @Test
    public void testRead() {
        this.lineReader.read(this.salesFileMock, this.line);
        Customer customer = new CustomerImpl("Jose da Silva", "2345675434544345", "Rural");

        Mockito
            .verify(this.salesFileMock, Mockito.times(1))
            .addCustomer(customer);
    }
}
