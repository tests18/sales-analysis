package silva.thiago.reader.line;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import silva.thiago.file.reader.line.LineReader;
import silva.thiago.file.reader.line.SaleLineReader;
import silva.thiago.model.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class SaleLineReaderTest {
    private Iterator<String> line;
    private SalesFile salesFileMock;
    private LineReader lineReader;
    private Sale sale;

    @BeforeEach
    public void setup() {
        String saleLine = "08ç[1-34-10,2-33-1.50,3-40-0.10]çPaulo";

        this.lineReader = new SaleLineReader();
        this.line = Arrays.asList(saleLine.split("ç")).iterator();
        this.salesFileMock = Mockito.mock(SalesFile.class);
    }

    @Test
    public void testRead() {
        this.lineReader.read(this.salesFileMock, this.line);
        Salesman salesman = new SalesmanImpl("Paulo", "3245678865434", 40000.99d);
        List<SaleItem> items = new ArrayList<>();

        items.add(new SaleItemImpl(1, 34, 10d));
        items.add(new SaleItemImpl(2, 33, 1.5d));
        items.add(new SaleItemImpl(3, 40, 0.1d));

        Sale sale = new SaleImpl(8, items, salesman);

        Mockito
            .verify(this.salesFileMock, Mockito.times(1))
            .addSale(sale);
    }
}
