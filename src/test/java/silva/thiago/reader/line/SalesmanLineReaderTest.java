package silva.thiago.reader.line;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import silva.thiago.file.reader.line.LineReader;
import silva.thiago.file.reader.line.SalesmanLineReader;
import silva.thiago.model.SalesFile;
import silva.thiago.model.Salesman;
import silva.thiago.model.SalesmanImpl;

import java.util.Arrays;
import java.util.Iterator;

public class SalesmanLineReaderTest {
    private Iterator<String> line;
    private SalesFile salesFileMock;
    private LineReader lineReader;

    @BeforeEach
    public void setup() {
        String salesmanLine = "1234567891234çPedroç50000";

        this.lineReader = new SalesmanLineReader();
        this.line = Arrays.asList(salesmanLine.split("ç")).iterator();
        this.salesFileMock = Mockito.mock(SalesFile.class);
    }

    @Test
    public void testRead() {
        this.lineReader.read(this.salesFileMock, this.line);
        Salesman salesman = new SalesmanImpl(" Pedro", "1234567891234", 50000d);
        Mockito
            .verify(this.salesFileMock, Mockito.times(1))
            .addSalesman(salesman);
    }
}
