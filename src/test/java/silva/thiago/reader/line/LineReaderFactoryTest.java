package silva.thiago.reader.line;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import silva.thiago.file.reader.line.CustomerLineReader;
import silva.thiago.file.reader.line.LineReaderFactory;
import silva.thiago.file.reader.line.SaleLineReader;
import silva.thiago.file.reader.line.SalesmanLineReader;

public class LineReaderFactoryTest {
    @Test
    public void testCreateReader() {
        Assertions.assertTrue(LineReaderFactory.createReader(001) instanceof SalesmanLineReader);
        Assertions.assertTrue(LineReaderFactory.createReader(002) instanceof CustomerLineReader);
        Assertions.assertTrue(LineReaderFactory.createReader(003) instanceof SaleLineReader);
    }
}
