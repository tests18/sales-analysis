package silva.thiago.reader;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import silva.thiago.file.reader.SalesFileReaderImpl;
import silva.thiago.model.*;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class SalesFileReaderTest {
    private SalesFileReaderImpl fileReader;
    private SalesFile salesFile;

    @BeforeEach
    public void setup() {
        this.fileReader = new SalesFileReaderImpl("ç");

        this.salesFile = new SalesFileImpl("sales.txt");

        final Salesman pedro = new SalesmanImpl("Pedro", "1234567891234", 50000d);
        final Salesman paulo = new SalesmanImpl("Paulo", "3245678865434", 40000.99d);

        this.salesFile.addSalesman(pedro);
        this.salesFile.addSalesman(paulo);
        this.salesFile.addCustomer(new CustomerImpl("Jose da Silva", "2345675434544345", "Rural"));
        this.salesFile.addCustomer(new CustomerImpl("Eduardo Pereira", "2345675433444345", "Rural"));

        List<SaleItem> pedroSaleItems = new ArrayList<>();
        pedroSaleItems.add(new SaleItemImpl(1, 10, 100d));
        pedroSaleItems.add(new SaleItemImpl(2, 30, 2.5d));
        pedroSaleItems.add(new SaleItemImpl(3, 40, 3.1d));

        this.salesFile.addSale(new SaleImpl(10, pedroSaleItems, pedro));

        List<SaleItem> pauloSaleItems = new ArrayList<>();
        pauloSaleItems.add(new SaleItemImpl(1, 34, 10d));
        pauloSaleItems.add(new SaleItemImpl(2, 33, 1.5d));
        pauloSaleItems.add(new SaleItemImpl(3, 40, 0.1d));

        this.salesFile.addSale(new SaleImpl(8, pauloSaleItems, paulo));
    }
    @Test
    public void testGetFile() {
        SalesFile salesFile = this.fileReader.getFile(Paths.get("data/in/sales.txt"));

        Assertions.assertNotNull(salesFile);
        Assertions.assertEquals("sales.txt", salesFile.getName());
        Assertions.assertEquals(2, salesFile.getCustomerData().size());
        Assertions.assertEquals(2, salesFile.getSalesmanData().size());
        Assertions.assertEquals(2, salesFile.getSalesData().size());

        Assertions.assertEquals(salesFile, this.salesFile);
    }
}
