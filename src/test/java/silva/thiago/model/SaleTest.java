package silva.thiago.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class SaleTest {
    private Sale sale;

    @BeforeEach
    public void setup() {
        final Salesman john = new SalesmanImpl("John", "47670230096", 5000d);
        this.sale = new SaleImpl(10, new ArrayList<>(), john);
    }

    @Test
    public void testGetSaleSum() {
        this.sale.getItems().add(new SaleItemImpl(1, 10, 100d));
        this.sale.getItems().add(new SaleItemImpl(2, 30, 2.5d));
        this.sale.getItems().add(new SaleItemImpl(3, 40, 3.1d));

        Assertions.assertEquals(1199d, this.sale.getSaleSum());
    }

    @Test
    public void getSaleSumWithNoItems() {
        Assertions.assertEquals(0, this.sale.getSaleSum());
    }
}
