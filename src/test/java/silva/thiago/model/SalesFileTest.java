package silva.thiago.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class SalesFileTest {
    private SalesFile salesFile;

    @BeforeEach
    public void setup() {
        this.salesFile = new SalesFileImpl("test.txt");

        final Salesman john = new SalesmanImpl("John", "47670230096", 5000d);
        final Salesman jane = new SalesmanImpl("Jane", "91617832006", 2500d);

        this.salesFile.addSalesman(john);
        this.salesFile.addSalesman(jane);
        this.salesFile.addCustomer(new CustomerImpl("Custormer 1", "15510609000188", "Sales"));
        this.salesFile.addCustomer(new CustomerImpl("Custormer 2    ", "02012899000198", "Sales"));

        List<SaleItem> saleJohnItems = new ArrayList<>();
        saleJohnItems.add(new SaleItemImpl(1, 10, 100d));
        saleJohnItems.add(new SaleItemImpl(2, 30, 2.5d));
        saleJohnItems.add(new SaleItemImpl(3, 40, 3.1d));

        this.salesFile.addSale(new SaleImpl(10, saleJohnItems, john));

        List<SaleItem> saleJaneItems = new ArrayList<>();
        saleJaneItems.add(new SaleItemImpl(1, 34, 10d));
        saleJaneItems.add(new SaleItemImpl(2, 33, 1.5d));
        saleJaneItems.add(new SaleItemImpl(3, 40, 0.1d));

        this.salesFile.addSale(new SaleImpl(8, saleJaneItems, jane));
    }

    @Test
    public void testGetWorstSalesman() {
        Salesman worst = this.salesFile.getWorstSalesman();
        Assertions.assertEquals(worst.getName(), "Jane");
    }

    @Test
    public void testGetMostExpensiveSale() {
        Sale mostExpensive = this.salesFile.getMostExpensiveSale();

        Assertions.assertEquals(10, mostExpensive.getId());
    }

    @Test
    public void testGetSalesmanByName() {
        Assertions.assertEquals("John", this.salesFile.getSalesmanByName("John").getName());
    }
}
