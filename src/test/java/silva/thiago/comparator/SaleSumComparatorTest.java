package silva.thiago.comparator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import silva.thiago.model.Sale;

import java.util.Comparator;

public class SaleSumComparatorTest {
    private Sale saleAMock;
    private Sale saleBMock;
    private Comparator saleSumComparator;

    @BeforeEach
    public void setup() {
        this.saleAMock = Mockito.mock(Sale.class);
        this.saleBMock = Mockito.mock(Sale.class);
        this.saleSumComparator = new SaleSumComparator();
    }


    @Test
    public void testCompareSaleAHigherThanSaleB() {
        Mockito.when(this.saleAMock.getSaleSum()).thenReturn(100d);
        Mockito.when(this.saleBMock.getSaleSum()).thenReturn(50d);

        Integer result = this.saleSumComparator.compare(this.saleAMock, this.saleBMock);

        Assertions.assertEquals(1, result);
    }

    @Test
    public void testCompareSaleBHigherThanSaleA() {
        Mockito.when(this.saleAMock.getSaleSum()).thenReturn(50d);
        Mockito.when(this.saleBMock.getSaleSum()).thenReturn(100d);

        Integer result = this.saleSumComparator.compare(this.saleAMock, this.saleBMock);

        Assertions.assertEquals(-1, result);
    }

    @Test
    public void testCompareSaleBHEqualsToSaleA() {
        Mockito.when(this.saleAMock.getSaleSum()).thenReturn(50d);
        Mockito.when(this.saleBMock.getSaleSum()).thenReturn(50d);

        Integer result = this.saleSumComparator.compare(this.saleAMock, this.saleBMock);

        Assertions.assertEquals(0, result);
    }
}
