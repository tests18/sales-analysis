package silva.thiago.reports;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import silva.thiago.model.SaleImpl;
import silva.thiago.model.SalesFile;
import silva.thiago.model.Salesman;
import silva.thiago.model.SalesmanImpl;

import java.util.ArrayList;
import java.util.Map;

public class FlatSaleReportTest {
    @Test
    public void testGetReport() {
        SalesFile salesFileMock = Mockito.mock(SalesFile.class);
        SalesReport salesReport = new FlatSaleReport();
        Salesman salesman = new SalesmanImpl("John", "05225154018", 3000d);

        Mockito.when(salesFileMock.getCustomerData()).thenReturn(new ArrayList<>());
        Mockito.when(salesFileMock.getSalesmanData()).thenReturn(new ArrayList<>());
        Mockito.when(salesFileMock.getWorstSalesman()).thenReturn(salesman);
        Mockito.when(salesFileMock.getMostExpensiveSale()).thenReturn(new SaleImpl(1, new ArrayList<>(), salesman));

        Map<String, String> data = salesReport.getReportData(salesFileMock);

        Mockito.verify(salesFileMock, Mockito.times(1)).getCustomerData();
        Mockito.verify(salesFileMock, Mockito.times(1)).getSalesmanData();
        Mockito.verify(salesFileMock, Mockito.times(1)).getMostExpensiveSale();
        Mockito.verify(salesFileMock, Mockito.times(1)).getWorstSalesman();

        Assertions.assertEquals(data.size(), 4);
        Assertions.assertTrue(data.values().contains("John"));
    }
}