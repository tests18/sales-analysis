package silva.thiago.model;

import java.util.Objects;

public class NaturalPersonImpl extends PersonImpl {
    private String CPF;

    public NaturalPersonImpl(String name, String CPF) {
        super(name);
        this.CPF = CPF;
    }

    @Override
    public String getRegistration() {
        return this.CPF;
    }

    @Override
    public void setRegistration(String registration) {
        this.CPF = registration;
    }

    @Override
    public boolean equals(Object o) {
        boolean equals = false;
        if (o instanceof  NaturalPersonImpl) {
            NaturalPersonImpl that = (NaturalPersonImpl) o;
            equals = Objects.equals(CPF, that.CPF);
        }
        return equals;
    }
}
