package silva.thiago.model;

import silva.thiago.comparator.SaleSumComparator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SalesFileImpl extends FileImpl implements SalesFile {
    private final List<Salesman> salesmen;
    private final List<Customer> customers;
    private final List<Sale> sales;

    public SalesFileImpl(String name) {
        super(name);
        this.salesmen = new ArrayList<>();
        this.customers = new ArrayList<>();
        this.sales = new ArrayList<>();
    }

    @Override
    public List<Salesman> getSalesmanData() {
        return new ArrayList<>(this.salesmen);
    }

    @Override
    public Salesman getSalesmanByName(String name) {
        return this.salesmen
                .stream()
                .filter(salesman -> salesman.getName().equals(name))
                .findAny()
                .orElse(null);
    }

    @Override
    public Salesman getWorstSalesman() {
        Map<Salesman, Double> salesmanSales = this.sales.stream()
            .collect(Collectors.groupingBy(
                sale -> sale.getSalesman(),
                Collectors.summingDouble(Sale::getSaleSum)
            ));

        Map.Entry<Salesman, Double> worst = salesmanSales
            .entrySet()
            .stream()
            .min(Map.Entry.comparingByValue(Double::compareTo))
            .get();

        return worst.getKey();
    }

    @Override
    public void addSalesman(Salesman salesman) {
        this.salesmen.add(salesman);
    }

    @Override
    public List<Customer> getCustomerData() {
        return new ArrayList<>(this.customers);
    }

    @Override
    public void addCustomer(Customer customer) {
        this.customers.add(customer);
    }

    @Override
    public List<Sale> getSalesData() {
        return new ArrayList<>(this.sales);
    }

    @Override
    public Sale getMostExpensiveSale() {
        return this.sales.stream().max(new SaleSumComparator()).get();
    }

    @Override
    public void addSale(Sale sale) {
        this.sales.add(sale);
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals = false;
        if(obj != null && obj instanceof SalesFileImpl) {
            final SalesFileImpl salesFile = (SalesFileImpl) obj;

            equals = super.equals(obj) &&
                    this.salesmen.equals(salesFile.salesmen) &&
                    this.customers.equals(salesFile.customers) &&
                    this.sales.equals(salesFile.sales);
        }
        return equals;
    }
}
