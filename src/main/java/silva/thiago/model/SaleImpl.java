package silva.thiago.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SaleImpl implements Sale {

    private Integer id;
    private final Salesman salesman;
    private List<SaleItem> items;

    public SaleImpl(Integer id, List<SaleItem> items, Salesman salesman) {

        this.id = id;
        this.salesman = salesman;

        this.items = new ArrayList<>(items);
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public void addItem(SaleItem item) {
        this.items.add(item);
    }

    @Override
    public List<SaleItem> getItems() {
        return this.items;
    }

    @Override
    public Salesman getSalesman() {
        return this.salesman;
    }

    @Override
    public Double getSaleSum() {
       return this.items.stream().collect(Collectors.summingDouble(SaleItem::getTotalPrice));
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals = false;
        if(obj != null && obj instanceof SaleImpl) {
            final SaleImpl sale = (SaleImpl) obj;

            equals = this.id == sale.id;
        }
        return equals;
    }
}