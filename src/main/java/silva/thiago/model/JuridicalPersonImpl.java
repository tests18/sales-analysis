package silva.thiago.model;

import java.util.Objects;

public class JuridicalPersonImpl extends PersonImpl  {
    private String CNPJ;

    public JuridicalPersonImpl(String name, String CNPJ) {
        super(name);
        this.CNPJ = CNPJ;
    }

    public String getRegistration() {
        return CNPJ;
    }

    public void setRegistration(String CNPJ) {
        this.CNPJ = CNPJ;
    }

    @Override
    public boolean equals(Object o) {
        boolean equals = false;
        if (o instanceof  JuridicalPersonImpl) {
            JuridicalPersonImpl that = (JuridicalPersonImpl) o;
            equals = Objects.equals(CNPJ, that.CNPJ);
        }
        return equals;
    }
}
