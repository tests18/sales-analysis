package silva.thiago.model;

public interface Salesman extends Person {
    public Double getSalary();

    public void setSalary(Double salary);
}
