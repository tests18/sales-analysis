package silva.thiago.model;

import java.util.List;

public interface SalesFile extends File {
    public List<Salesman> getSalesmanData();

    public Salesman getSalesmanByName(String name);

    public Salesman getWorstSalesman();

    public void addSalesman(Salesman salesman);

    public List<Customer> getCustomerData();

    public void addCustomer(Customer customer);

    public List<Sale> getSalesData();

    public Sale getMostExpensiveSale();

    public void addSale(Sale sale);
}
