package silva.thiago.model;

import java.util.Objects;

public class FileImpl implements File {
    private String name;

    public FileImpl(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o instanceof FileImpl) {
            FileImpl file = (FileImpl) o;
            return Objects.equals(name, file.name);
        }

        return false;
    }
}
