package silva.thiago.model;

public interface Customer {
    public String getBusinessArea();
    public void setBusinessArea(String businessArea);
}
