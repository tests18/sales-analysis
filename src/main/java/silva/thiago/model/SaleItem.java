package silva.thiago.model;

public interface SaleItem {
    public Integer getId();

    public void setId(Integer id);

    public Integer getQuantity();

    public void setQuantity(Integer quantity);

    public Double getPrice();

    public void setPrice(Double price);

    public Double getTotalPrice();
}
