package silva.thiago.model;

public class SaleItemImpl implements SaleItem {
    private Integer id;
    private Integer quantity;
    private Double price;

    public SaleItemImpl(Integer id, Integer quantity, Double price) {
        this.id = id;
        this.quantity = quantity;
        this.price = price;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public Integer getQuantity() {
        return this.quantity;
    }

    @Override
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public Double getPrice() {
        return this.price;
    }

    @Override
    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public Double getTotalPrice() {
        return this.price * quantity;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals = false;
        if(obj != null && obj instanceof SaleItemImpl) {
            final SaleItemImpl saleItem = (SaleItemImpl) obj;

            equals = this.id == saleItem.id;
        }
        return equals;
    }
}
