package silva.thiago.model;

public class SalesmanImpl extends NaturalPersonImpl implements Salesman {
    private Double salary;

    public SalesmanImpl(String name, String CPF, Double salary) {
        super(name, CPF);
        this.salary = salary;
    }

    @Override
    public Double getSalary() {
        return this.salary;
    }

    @Override
    public void setSalary(Double salary) {
        this.salary = salary;
    }
}
