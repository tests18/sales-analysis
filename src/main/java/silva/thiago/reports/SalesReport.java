package silva.thiago.reports;

import silva.thiago.model.SalesFile;

import java.util.Map;

public interface SalesReport {
    public Map<String, String> getReportData(SalesFile file);
}
