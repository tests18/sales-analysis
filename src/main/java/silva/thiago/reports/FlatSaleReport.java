package silva.thiago.reports;

import silva.thiago.model.SalesFile;

import java.util.HashMap;
import java.util.Map;

public class FlatSaleReport implements SalesReport {
    @Override
    public Map<String, String> getReportData(SalesFile file) {
        Map<String, String> reportData = new HashMap();

        reportData.put("Customers quantity",Integer.toString(file.getCustomerData().size()));
        reportData.put("Salesman quantity", Integer.toString(file.getSalesmanData().size()));
        reportData.put("Most expensive sale ID", Integer.toString(file.getMostExpensiveSale().getId()));
        reportData.put("Worst salesman name", file.getWorstSalesman().getName());

        return reportData;
    }
}
