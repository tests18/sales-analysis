package silva.thiago.comparator;

import silva.thiago.model.Sale;

import java.util.Comparator;

public class SaleSumComparator implements Comparator<Sale> {
    @Override
    public int compare(Sale left, Sale right) {
        Double leftSum = left.getSaleSum();
        Double rightSum = right.getSaleSum();

        return leftSum.compareTo(rightSum);
    }
}
