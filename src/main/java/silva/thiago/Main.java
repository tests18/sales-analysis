package silva.thiago;

import silva.thiago.file.watcher.SalesFileWatcher;
import silva.thiago.file.watcher.SalesFileWatcherImpl;

import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {
    public static void main(String[] args) {
        final String homepath = System.getenv("HOMEPATH");
        final Path inDir = Paths.get(homepath, "data", "in");
        final Path outDir = Paths.get(homepath, "data", "out");
        final SalesFileWatcher salesFileWatcher = new SalesFileWatcherImpl();

        salesFileWatcher.watch(inDir, outDir);
    }
}
