package silva.thiago.file.reader;

import silva.thiago.file.reader.line.LineReader;
import silva.thiago.file.reader.line.LineReaderFactory;
import silva.thiago.model.SalesFile;
import silva.thiago.model.SalesFileImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class SalesFileReaderImpl implements FileReader {
    String separator;

    public SalesFileReaderImpl(String separator) {
        super();
        this.separator = separator;
    }

    @Override
    public SalesFile getFile(Path filePath) {
        SalesFile salesFile = null;
        try {
            BufferedReader reader = Files.newBufferedReader(filePath);
            salesFile = this.processFile(reader, filePath);
            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return salesFile;
    }

    private SalesFile processFile(BufferedReader reader, Path filePath) {
        SalesFile salesFile = new SalesFileImpl(filePath.getFileName().toString());

        try {
            while(reader.ready()) {
               List<String> lineParts = Arrays.asList(reader.readLine().split(this.separator));
                Iterator<String> partsIterator = lineParts.iterator();
                Integer format = Integer.parseInt(partsIterator.next());
                LineReader lineReader = LineReaderFactory.createReader(format);
                lineReader.read(salesFile, partsIterator);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return salesFile;
    }
}
