package silva.thiago.file.reader.line;

public class LineReaderFactory {
    private static final int SALESMAN_FORMAT = 001;
    private static final int CUSTOMER_FORMAT = 002;
    private static final int SALE_FORMAT = 003;

    public static LineReader createReader(Integer format) {
        LineReader reader = null;

        switch (format) {
            case SALESMAN_FORMAT: {
                reader = new SalesmanLineReader();
                break;
            }
            case CUSTOMER_FORMAT: {
                reader = new CustomerLineReader();
                break;
            }
            case SALE_FORMAT: {
                reader = new SaleLineReader();
                break;
            }
        }

        return reader;
    }
}
