package silva.thiago.file.reader.line;

import silva.thiago.model.CustomerImpl;
import silva.thiago.model.SalesFile;

import java.util.Iterator;

public class CustomerLineReader implements LineReader {
    @Override
    public void read(SalesFile file, Iterator<String> line) {
        final String CNPJ = line.next();
        final String name = line.next();
        final String businessArea = line.next();

        file.addCustomer(new CustomerImpl(name, CNPJ, businessArea));
    }
}
