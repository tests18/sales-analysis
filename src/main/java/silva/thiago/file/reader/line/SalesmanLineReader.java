package silva.thiago.file.reader.line;

import silva.thiago.model.SalesFile;
import silva.thiago.model.SalesmanImpl;

import java.util.Iterator;

public class SalesmanLineReader implements LineReader {
    @Override
    public void read(SalesFile file, Iterator<String> line) {
        final String CPF = line.next();
        final String name = line.next();
        final Double salary = Double.parseDouble(line.next());

        file.addSalesman(new SalesmanImpl(name, CPF, salary));
    }
}
