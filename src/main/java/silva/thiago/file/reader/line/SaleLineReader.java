package silva.thiago.file.reader.line;

import silva.thiago.model.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class SaleLineReader implements LineReader {
    @Override
    public void read(SalesFile file, Iterator<String> line) {
        final Integer id = Integer.parseInt(line.next());
        final List<SaleItem> saleItems = this.processSalesItems(line.next());
        final Salesman salesman = file.getSalesmanByName(line.next());

        file.addSale(new SaleImpl(id, saleItems, salesman));
    }

    private List<SaleItem> processSalesItems(String salesInfos) {
        final List<SaleItem> saleItems = new ArrayList<>();

        Arrays
            .asList(salesInfos.substring(1, salesInfos.length() - 1).split(","))
            .stream()
            .forEach(saleItemStr -> saleItems.add(this.processSaleItem(saleItemStr)));

        return saleItems;
    }

    private SaleItem processSaleItem(String saleInfosStr) {
        Iterator<String> saleInfos = Arrays.asList(saleInfosStr.split("-")).iterator();
        final Integer id = Integer.parseInt(saleInfos.next());
        final Integer quantity = Integer.parseInt(saleInfos.next());
        final Double price = Double.parseDouble(saleInfos.next());

        return new SaleItemImpl(id, quantity, price);
    }
}
