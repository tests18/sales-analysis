package silva.thiago.file.reader.line;

import silva.thiago.model.SalesFile;

import java.util.Iterator;

public interface LineReader {
    void read(SalesFile file, Iterator<String> line);
}
