package silva.thiago.file.reader;

import silva.thiago.model.File;

import java.nio.file.Path;

public interface FileReader {
    public File getFile(Path filePath);
}
