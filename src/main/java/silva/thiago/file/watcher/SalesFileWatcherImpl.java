package silva.thiago.file.watcher;

import silva.thiago.file.reader.SalesFileReaderImpl;
import silva.thiago.file.writer.FileReportWriter;
import silva.thiago.file.writer.ReportWriter;
import silva.thiago.model.SalesFile;
import silva.thiago.reports.FlatSaleReport;
import silva.thiago.reports.SalesReport;

import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;

public class SalesFileWatcherImpl implements SalesFileWatcher {
    private final static String[] FILE_EXTENSIONS = new String[] {"dat"};
    public void watch(Path inDir, Path outDir) {
        final FileWatcher fileWatcher = new FileWatcherImpl();

        fileWatcher.addListener(StandardWatchEventKinds.ENTRY_MODIFY, file -> {
            final SalesFileReaderImpl fileReader = new SalesFileReaderImpl("ç");
            final SalesReport report = new FlatSaleReport();

            SalesFile salesFile =  fileReader.getFile(file);
            final ReportWriter writer = new FileReportWriter(
                outDir.toFile(),
                salesFile.getName()
            );
            writer.write(report.getReportData(salesFile));
        });

        fileWatcher.watch(inDir, FILE_EXTENSIONS);
    }
}
