package silva.thiago.file.watcher;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class FileWatcherImpl implements FileWatcher {
        Map<WatchEvent.Kind, Consumer<Path>> listeners = new HashMap<>();

    @Override
    public void watch(Path filePath, String[] fileExtensions) {
        final List<String> fileExtensionsList = Arrays.asList(fileExtensions);
        try {
            final WatchService watcher = filePath.getFileSystem().newWatchService();
            filePath.register(
                    watcher,
                    this.listeners.keySet().toArray(new WatchEvent.Kind[this.listeners.size()])
            );

            WatchKey key;

            while ((key = watcher.take()) != null) {
                key.pollEvents()
                    .stream()
                    .filter(event -> {
                        String fileName = event.context().toString();
                        String extension = fileName.toLowerCase().substring(fileName.length() -3, fileName.length());
                        return fileExtensionsList.contains(extension);
                    })
                    .filter(event -> this.listeners.containsKey(event.kind()))
                    .forEach(event -> processEvent(filePath, event));

                key.reset();
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void processEvent(Path filePath, WatchEvent<?> event) {
        try {
            Thread.sleep(1000);
            this.listeners.get(event.kind()).accept(filePath.resolve((Path) event.context()));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addListener(WatchEvent.Kind kind, Consumer<Path> listener) {
        this.listeners.put(kind, listener);
    }

    public Map<WatchEvent.Kind, Consumer<Path>> getListeners() {
        return this.listeners;
    }
}
