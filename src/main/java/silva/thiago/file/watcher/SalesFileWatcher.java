package silva.thiago.file.watcher;

import java.nio.file.Path;

public interface SalesFileWatcher {
    public void watch(Path inDir, Path outDir);
}
