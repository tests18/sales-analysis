package silva.thiago.file.watcher;

import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.util.Map;
import java.util.function.Consumer;

public interface FileWatcher {
    public void watch(Path dir, String[] fileExtensions);
    public void addListener(WatchEvent.Kind kind, Consumer<Path> listener);
    public Map<WatchEvent.Kind, Consumer<Path>> getListeners();
}
