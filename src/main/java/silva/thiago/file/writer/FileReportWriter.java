package silva.thiago.file.writer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

public class FileReportWriter implements ReportWriter {
    private static final String SUFFIX = ".done.dat";
    private File dir;
    private String filename;

    public FileReportWriter(File dir, String filename) {
        this.dir = dir;
        this.filename = filename;
    }

    @Override
    public void write(Map<String, String> data) {

        if (!this.dir.exists()) {
            this.dir.mkdirs();
        }

        final File writeFile = new File(
            this.dir,
            this.filename.substring(0, this.filename.length() - 4).concat(SUFFIX)
        );

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(writeFile));

            data.entrySet()
                .stream()
                .map(entry -> String.format(entry.getKey().concat(": %s\n"), entry.getValue()))
                .forEach(line -> this.writeLine(writer, line));

            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeLine(BufferedWriter writer, String line) {
        try {
            writer.write(line);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
