package silva.thiago.file.writer;

import java.util.Map;

public interface ReportWriter {
    public void write(Map<String, String> data);
}
